import 'package:badges/src/badge.dart' as BadgesPackage; // Use an alias for the badges package
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop/cart_model.dart';
import 'package:shop/provider.dart';
import 'package:shop/screen.dart';
import 'package:shop/db.dart';

class ProductListScreen extends StatefulWidget {
  const ProductListScreen({Key? key}) : super(key: key);

  @override
  _ProductListScreenState createState() => _ProductListScreenState();
}

class _ProductListScreenState extends State<ProductListScreen> {
  List<String> productName = ['Balder gate 3', 'GTA V', 'Armored Core VI', 'Final Fantasy XVI', 'Final Fantasy VII Remake', 'Final Fantasy VII Remake Intergrade', 'StarField', 'Resident Evil 4 Remake', 'The Legend of Zelda: Tears of the Kingdom', 'Diablo IV',] ;
  List<String> productUnit = ['Rates', 'Rates', 'Rates', 'Rates', 'Rates', 'Rates', 'Rates', 'Rates', 'Rates', 'Rates',] ;
  List<int> productPrice = [59, 27, 59, 64, 59, 69, 69, 59, 59, 69 ] ;
  List<String> productImage = [
    'https://tryhardguides.com/wp-content/uploads/2023/07/baldurs-gate-3-key-art-1024x576.jpg',
    'https://cdn.akamai.steamstatic.com/steam/apps/271590/header.jpg?t=1678296348',
    'https://sm.ign.com/t/ign_in/screenshot/default/mg-7_eqgn.1280.png',
    'https://image.api.playstation.com/vulcan/ap/rnd/202211/3007/JnzRCl2Yj208yuJoSfoGXMGt.jpg',
    'https://www.appdisqus.com/wp-content/uploads/2021/12/ff7-remake-upgrade-cover.jpg',

    'https://cdn1.epicgames.com/offer/6f43ab8025ad42d18510aa91e9eb688b/EGS_FINALFANTASYVIIREMAKEINTERGRADE_SquareEnix_S1_2560x1440-85f829541a833442eaace75d02e0f07d',
    'https://static.standard.co.uk/2023/08/17/11/starfield-1.jpg?width=1200&height=630&fit=crop',
    'https://i.ytimg.com/vi/Id2EaldBaWw/maxresdefault.jpg',
    'https://fs-prod-cdn.nintendo-europe.com/media/images/10_share_images/games_15/nintendo_switch_4/2x1_NSwitch_TloZTearsOfTheKingdom_Gamepage_image1600w.jpg',
    'https://img.tapimg.net/market/images/27378e30a6856b6778b780667077f816.png?imageView2/2/w/720/h/720/q/80/format/jpg/interlace/1/ignore-error/1',
  ] ;

  DBHelper? dbHelper = DBHelper();

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<CartProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Product List'),
        centerTitle: true,
        actions: [
          InkWell(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => CartScreen()));
            },
            child: Center(
              child: BadgesPackage.Badge( // Use the alias to specify the Badge class from the badges package
                showBadge: true,
                badgeContent: Consumer<CartProvider>(
                  builder: (context, value, child) {
                    return Text(value.getCounter().toString(), style: TextStyle(color: Colors.white));
                  },
                ),
                animationDuration: Duration(milliseconds: 300),
                child: Icon(Icons.shopping_cart_sharp),
              ),
            ),
          ),
          SizedBox(width: 20.0)
        ],
      ),

      drawer: Drawer(
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: [
            const UserAccountsDrawerHeader(
              decoration: BoxDecoration(color: Colors.blue),
              accountName: Text(
                "Steam Store",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 19,
                ),
              ),
              accountEmail: Text(
                "support@steampowered.com",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              currentAccountPicture: CircleAvatar(
                backgroundImage: AssetImage('assets/images/shopstore.png'),
                radius: 60,
              ),
            ),

            AboutListTile( // <-- SEE HERE
              icon: Icon(
                Icons.account_circle,
              ),
              child: Text('ABOUT'),
              applicationIcon: Icon(
                Icons.account_circle,
              ),
              applicationName: 'About',
              aboutBoxChildren: [
                Image.asset("assets/images/pooh.png",width: 150,height: 160,),
                SizedBox(height: 5,),
                Text("IDTM",textAlign: TextAlign.center,style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),),
                Text("รหัสนักศึกษา : 6450110009",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),),
                Text("ชื่อนักศึกษา : ภควัต พรมเอี่ยม",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),),
                SizedBox(height: 20,),
                Image.asset("assets/images/note.jpg",width: 150,height: 160,),
                SizedBox(height: 5,),
                Text("IDTM",textAlign: TextAlign.center,style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),),
                Text("รหัสนักศึกษา : 6450110013",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),),
                Text("ชื่อนักศึกษา : สิทธิศักดิ์ ชูเล็ก",style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),),
              ],
            ),
          ],
        ),
      ),

      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
                itemCount: productName.length,
                itemBuilder: (context, index){
                  return Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Image(
                                height: 100,
                                width: 100,
                                image: NetworkImage(productImage[index].toString()),
                              ),
                              SizedBox(width: 10,),
                              Expanded(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(productName[index].toString() ,
                                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(height: 5,),
                                    Text(productUnit[index].toString() +" "+r"$"+ productPrice[index].toString() ,
                                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
                                    ),
                                    SizedBox(height: 5,),
                                    Align(
                                      alignment: Alignment.centerRight,
                                      child: InkWell(
                                        onTap: (){
                                          print(index);
                                          print(index);
                                          print(productName[index].toString());
                                          print( productPrice[index].toString());
                                          print( productPrice[index]);
                                          print('1');
                                          print(productUnit[index].toString());
                                          print(productImage[index].toString());

                                          dbHelper!.insert(
                                              Cart(
                                                  id: index,
                                                  productId: index.toString(),
                                                  productName: productName[index].toString(),
                                                  initialPrice: productPrice[index],
                                                  productPrice: productPrice[index],
                                                  quantity: 1,
                                                  unitTag: productUnit[index].toString(),
                                                  image: productImage[index].toString())
                                          ).then((value){

                                            cart.addTotalPrice(double.parse(productPrice[index].toString()));
                                            cart.addCounter();

                                            final snackBar = SnackBar(backgroundColor: Colors.green,content: Text('Product is added to cart'), duration: Duration(seconds: 1),);

                                            ScaffoldMessenger.of(context).showSnackBar(snackBar);

                                          }).onError((error, stackTrace){
                                            print("ERROR"+error.toString());
                                            final snackBar = SnackBar(backgroundColor: Colors.red ,content: Text('Product is already added in cart'), duration: Duration(seconds: 1));

                                            ScaffoldMessenger.of(context).showSnackBar(snackBar);
                                          });
                                        },
                                        child:  Container(
                                          height: 35,
                                          width: 100,
                                          decoration: BoxDecoration(
                                              color: Colors.blue ,
                                              borderRadius: BorderRadius.circular(12)
                                          ),
                                          child: const Center(
                                            child:  Text('Add to cart' , style: TextStyle(color: Colors.white),),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )

                            ],
                          )
                        ],
                      ),
                    ),
                  );
                }),
          ),

        ],
      ),
    );
  }
}
